package devops;

import java.util.Random;

public class App {

    public static void main(String[] args) {
        Random choixpiece = new Random();

        Joueur joueur1 = new Joueur("Alexandre");
        Joueur joueur2 = new Joueur("Paul");

        System.out.println("*********************************");
        System.out.println( joueur1.getPseudo() + " s'est connecté au jeu.");
        System.out.println(joueur2.getPseudo() + " s'est connecté au jeu.");
        System.out.println("*********************************");

        System.out.println("Les joueurs font leur choix...");

        int choixJoueur1 = choixpiece.nextInt(2) + 1;
        if(choixJoueur1 == 1){
            System.out.println("Le premier joueur a choisi pile.");
        }
        else{
            System.out.println("Le premier joueur a choisi face.");
        }

        int choixJoueur2 = choixpiece.nextInt(2) + 1;
        if(choixJoueur2 == 1){
            System.out.println("Le deuxième joueur a choisi pile.");
        }
        else{
            System.out.println("Le deuxième joueur a choisi face.");
        }
        System.out.println("*********************************");


        int lancerPiece = joueur1.startGame();

        if (choixJoueur1 == lancerPiece) {
            joueur1.winGame();
        }
        else {
            joueur1.loseGame();
        }
        
        if (choixJoueur2 == lancerPiece) {
            joueur2.winGame();
        }
        else {
            joueur2.loseGame();
        }

        System.out.println("La pièce est lancée...");

        if(lancerPiece == 1){
            System.out.println("Le résultat est pile.");
        }
        else{
            System.out.println("Le résultat est face.");
        }
        System.out.println("*********************************");
        System.out.println("Tableau des scores :");
        System.out.println(joueur1.toString());
        System.out.println(joueur2.toString());
        System.out.println("*********************************");
    }
}
