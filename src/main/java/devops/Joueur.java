package devops;

import java.util.Random;

public class Joueur {

    Random identifiant = new Random();
    Random pileouface = new Random();
    
    private final int id;
    private final String pseudo;
    private int score;
    private int piece;

    public Joueur(String pseudo) {
        this.id = identifiant.nextInt(9999999);
        this.pseudo = pseudo;
        this.score = 0;
    }
    
    public int startGame(){
        piece = pileouface.nextInt(2) +1;
        return piece;
    }
    
    public void winGame(){
        this.score +=1;
    }
    
    public void loseGame(){
        if(this.score == 0){
            this.score = 0;
        }
        else{
            this.score -= 1;
        }
    }

    public String getPseudo() {
        return pseudo;
    }

    @Override
    public String toString() {
        return "Joueur " + id + " {" + " pseudo : " + pseudo + ", score : " + score + " }";
    }
}
