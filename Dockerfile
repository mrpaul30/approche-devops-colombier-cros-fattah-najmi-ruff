FROM java:8

COPY . /var/www/java

WORKDIR /var/www/java

RUN javac src/main/java/devops/Joueur.java && javac src/main/java/devops/App.java

CMD ["java", "App"]  